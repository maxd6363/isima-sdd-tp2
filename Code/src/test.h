/**
 * @brief      Définition des tests sur les files et les piles
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef TEST_H
#define TEST_H

#include "bool.h"

#define STACK_MAX_SIZE 5
#define QUEUE_MAX_SIZE 5

/**
 * @brief Teste l'empilage dans une pile
 * 
 * @return le test à réussi 
 */
bool testPushStack(void);

/**
 * @brief Teste le dépilage d'une pile
 * 
 * @return le test à réussi  ou pas
 */
bool testPopStack(void);

/**
 * @brief Teste la récupération du haut d'une pile
 * 
 * @return le test à réussi  ou pas
 */
bool testTopStack(void);

/**
 * @brief Teste le redimentionnement de la pile
 *
 * @return le test a réussi ou pas
 */
bool testResizeStack(void);

/**
 * @brief Teste l'enfilage dans une file
 * 
 * @return le test à réussi  ou pas
 */
bool testPushQueue(void);

/**
 * @brief Teste le défilage d'une file
 * 
 * @return le test à réussi  ou pas
 */
bool testPullQueue(void);

/**
 * @brief Teste la récupération de la queue d'une file
 * 
 * @return le test à réussi  ou pas
 */
bool testEndQueue(void);

/**
 * @brief Affiche le résultat d'un test "test" 
 * 
 * @param test Le test
 */
bool printResult(bool test);

/**
 * @brief Lance tous les tests
 * 
 */
bool runTests(void);

#endif
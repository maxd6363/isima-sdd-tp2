/**
 * @brief      Implémentation du type erreur et des fonctions associées
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "error.h"
#include "color.h"
#include <stdio.h>

/**
 * @brief      Fonction pour logger les erreurs
 *
 * @param[in]  err   L'erreur
 */
void logError(error_t err) {
#ifdef VERBOSE

	switch (err) {
	case OK:
		fprintf(stderr, GRE "OK");
		break;
	case ERROR:
		fprintf(stderr, RED "ERROR : Erreur non spécifié");
		break;
	case FILE_NOT_FOUND:
		fprintf(stderr, RED "ERROR : Fichier non trouvé");
		break;
	case ALLOC_ERROR:
		fprintf(stderr, RED "ERROR : Erreur allocation");
		break;
	case OUT_OF_BOUNDS:
		fprintf(stderr, RED "ERROR : Index hors limite");
		break;
	case NULL_POINTER:
		fprintf(stderr, RED "ERROR : Pointer null");
		break;
	case NOT_IMPLEMENTED_YET:
		fprintf(stderr, RED "ERROR : Pas encore implementé");
		break;
	case STACK_OVERFLOW:
		fprintf(stderr, RED "ERROR : La pile est pleine");
		break;
	case STACK_EMPTY:
		fprintf(stderr, RED "ERROR : La pile est vide");
		break;
	case QUEUE_OVERFLOW:
		fprintf(stderr, RED "ERROR : La file est pleine");
		break;
	case QUEUE_EMPTY:
		fprintf(stderr, RED "ERROR : La file est vide");
		break;
	default:
		fprintf(stderr, RED "ERROR");
		break;
	}
	printf("\n" RESET);

#endif
}

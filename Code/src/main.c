/**
 * @brief      Point d'entrée du programme
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cnp.h"
#include "queue.h"
#include "stack.h"
#include "test.h"
#include "error.h"
#include "color.h"


/**
 * @brief      Point d'entrée du programme
 *
 * @param[in]  argc  Le nombre d'argument
 * @param      argv  Le tableau contenant les arguments
 *
 * @return     Code d'erreur du programme
 */
int main(void) {
	int n = 31;
	int p = 19;
	printf("\nCNP(%d,%d)\n", n, p);
	//cnpTestPerformance(n, p);

	if(runTests())
		printf(GRE "\n\nTous les tests ont étés réussis !\n" RESET);
	else
		printf(RED "\n\nCertains tests n'ont pas étés réussis ...\n" RESET);

	
	return 0;
}
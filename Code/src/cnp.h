/**
 * @brief      Déclaration des fonctions associés à l'algorithme CNP
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef CNP_H
#define CNP_H

/**
 * @brief Algorithme CNP récursif
 * 
 * @param n N
 * @param p P
 * @return int Résultat
 */
int cnpRecursif(int n, int p);

/**
 * @brief Affiche la trace de l'algorithme CNP
 * 
 * @param n N
 * @param p P
 * @param depth Profondeur de la récurssion, toujour mettre 0
 * @return int Résultat
 */
int cnpTrace(int n, int p, int depth);

/**
 * @brief Algorithme CNP récursif mais sans la récursion terminal
 * 
 * @param n N
 * @param p P
 * @return int Résultat
 */
int cnpNonTerminal(int n, int p);

/**
 * @brief Algorithme CNP itératif
 * 
 * @param n N
 * @param p P
 * @return int Résultat
 */
int cnpNonRecursif(int n, int p);

/**
 * @brief Permet d'afficher le nombre de seconde de calcule pour le récursif et l'itératif
 * 
 * @param n N
 * @param p P
 */
void cnpTestPerformance(int n, int p);

#endif
/**
 * @brief      Implémentation des fonctions associés à l'algorithme CNP
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */
#include "cnp.h"

#include <stdio.h>
#include <time.h>

#include "bool.h"
#include "stack.h"

#define STACK_SIZE 1024

/**
 * @brief Algorithme CNP récursif
 * 
 * @param n N
 * @param p P
 * @return int Résultat
 */
int cnpRecursif(int n, int p) {
	if (n == p || p == 0) {
		return 1;
	}
	return cnpRecursif(n - 1, p) + cnpRecursif(n - 1, p - 1);
}

/**
 * @brief Affiche la trace de l'algorithme CNP
 * 
 * @param n N
 * @param p P
 * @param depth Profondeur de la récursion, toujours mettre 0
 * @return int Résultat
 */
int cnpTrace(int n, int p, int depth) {
	for (int i = 0; i < depth; i++) {
		printf("│\t\t");
	}

	printf("├────────── cnp(%d,%d)", n, p);
	if (n == p || p == 0) {
		printf(" = 1\n");
		return 1;
	}
	printf("\n");
	return cnpTrace(n - 1, p, depth + 1) + cnpTrace(n - 1, p - 1, depth + 1);
}

/**
 * @brief Algorithme CNP récursif mais sans la récursion terminale
 * 
 * @param n N
 * @param p P
 * @return int Résultat
 */
int cnpNonTerminal(int n, int p) {
	int ni = n;
	int pi = p;
	int aux = 0;
	while (ni != pi && pi != 0) {
		aux += cnpNonTerminal(ni - 1, pi);
		ni--;
		pi--;
	}
	return aux + 1;
}

/**
 * @brief Algorithme CNP itératif
 * 
 * @param n N
 * @param p P
 * @return int Résultat
 */
int cnpNonRecursif(int n, int p) {
	int ni = n;
	int pi = p;
	int aux = 0;
	stack_t stack;
	bool end = false;
	stackInit(&stack, STACK_SIZE);
	while (!end) {
		if (ni != pi && pi != 0) {
			stackPushLight(&stack, ni);
			stackPushLight(&stack, pi);
			ni--;
			pi--;
		} else {
			aux++;
			if (isStackEmpty(stack)) {
				end = true;
			} else {
				stackPopLight(&stack, &pi);
				stackPopLight(&stack, &ni);
				ni--;
			}
		}
	}
	stackFree(&stack);
	return aux;
}

/**
 * @brief Permet d'afficher le nombre de seconde de calcul pour le récursif et l'itératif
 * 
 * @param n N
 * @param p P
 */
void cnpTestPerformance(int n, int p) {
	time_t now;
	printf("Récursif : ");
	now = time(0);
	cnpRecursif(n, p);
	printf("%ld secondes\n", time(0) - now);

	printf("Itératif : ");
	now = time(0);
	cnpNonRecursif(n, p);
	printf("%ld secondes\n", time(0) - now);
}
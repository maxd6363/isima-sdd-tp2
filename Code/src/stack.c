/**
 * @brief      Implémentation du type pile
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include "stack.h"

#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Initialise la pile à zero avec sizeMax places
 * 
 * @param out La pile
 * @param sizeMax La taille maximale 
 * @return error_t L'erreur
 */
error_t stackInit(stack_t *out, int sizeMax) {
	error_t error = OK;

	if (out == NULL) {
		error = NULL_POINTER;
	} else if (sizeMax < 0) {
		error = OUT_OF_BOUNDS;
	} else {
		out->sizeMax = sizeMax;
		out->top = -1;
		out->data = (data_stack_t *)malloc(sizeMax * sizeof(data_stack_t));
		if (out->data == NULL) {
			error = ALLOC_ERROR;
			out->sizeMax = 0;
		}
	}
	return error;
}

/**
 * @brief Affiche une pile
 * 
 * @param stack La pile
 */
void stackPrint(stack_t stack) {
	int i;
	#ifdef VERBOSE
		printf("TOP : %d\n", stack.top);
		printf("MAX : %d\n", stack.sizeMax);
	#endif

	for (i = 0; i <= stack.top; i++) {
		printf(PILE_FORMAT " ", stack.data[i]);
	}
	printf("\n");
}

/**
 * @brief Insère une valeur dans la pile
 * 
 * @param stack La pile
 * @param value La valeur
 * @return error_t L'erreur
 */
error_t stackPush(stack_t *stack, data_stack_t value) {
	error_t error = OK;
	if (stack == NULL) {
		error = NULL_POINTER;
	} else if (isStackFull(*stack)) {
		error = STACK_OVERFLOW;
	} else {
		stackPushLight(stack,value);
	}
	return error;
}

/**
 * @brief Sort une valeur de la pile
 * 
 * @param stack La pile
 * @param out La valeur récupérée
 * @return error_t L'erreur
 */
error_t stackPop(stack_t *stack, data_stack_t *out) {
	error_t error = OK;
	if (stack == NULL) {
		error = NULL_POINTER;
	} else if (isStackEmpty(*stack)) {
		error = STACK_EMPTY;
	} else {
		if (out != NULL) {
			//pour pouvoir pop la pile sans avoir à forcément récupérer d'élément
			*out = stack->data[stack->top];
		}
		stack->top = stack->top - 1;
	}
	return error;
}

/**
 * @brief Vérifie si la pile est pleine
 * 
 * @param stack La pile à tester
 * @return bool Le résultat
 */
bool isStackFull(stack_t stack) {
	return stack.top >= stack.sizeMax - 1;
}

/**
 * @brief Vérifie si la pile est vide
 * 
 * @param stack La pile à tester
 * @return bool Le résultat
 */
bool isStackEmpty(stack_t stack) {
	return stack.top <= -1;
}

/**
 * @brief Libère la mémoire utilisée par une pile
 * 
 * @param stack La pile à libérer
 * @return error_t L'erreur
 */
error_t stackFree(stack_t *stack) {
	if (stack != NULL) {
		stack->sizeMax = 0;
		stack->top = -1;
		if (stack->data != NULL) {
			free(stack->data);
		}
	}
	return OK;
}

/**
 * @brief Permet de changer la taille maximal de la pile
 * 
 * @param stack La pile
 * @param sizeMax La nouvelle taille max
 * @return error_t L'erreur
 */
error_t stackResize(stack_t *stack, int sizeMax) {
	error_t error = OK;
	if (stack == NULL) {
		error = NULL_POINTER;
	} else if (sizeMax < 0) {
		error = OUT_OF_BOUNDS;
	} else {
		stack->data = realloc(stack->data, sizeMax * sizeof(data_stack_t));
		stack->sizeMax = sizeMax;
		if (stack->data == NULL) {
			error = ALLOC_ERROR;
			stack->sizeMax = 0;
			stack->top = -1;
		}
	}
	return error;
}

/**
 * @brief Récupère l'élément en haut de la pile
 * 
 * @param stack La pile
 * @param top L'élément en haut
 * @return error_t L'erreur
 */
error_t stackTop(stack_t stack, data_stack_t *top) {
	error_t error = STACK_EMPTY;
	if (!isStackEmpty(stack)) {
		*top = stack.data[stack.top];
		error = OK;
	}
	return error;
}

/**
 * @brief Insère une valeur dans la pile, sans aucun test préalable, plus rapide mais non protégé
 * 
 * @param stack La pile
 * @param value La valeur
 * @return error_t L'erreur
 */
void stackPushLight(stack_t *stack, data_stack_t value) {
	stack->top = stack->top + 1;
	stack->data[stack->top] = value;
}

/**
 * @brief Sort une valeur de la pile, sans aucun test préalable, plus rapide mais non protégé
 * 
 * @param stack La pile
 * @param value La valeur
 * @return error_t L'erreur
 */
void stackPopLight(stack_t *stack, data_stack_t *out) {
	*out = stack->data[stack->top];
	stack->top = stack->top - 1;
}

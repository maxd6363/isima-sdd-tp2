#include "test.h"

#include "color.h"
#include "queue.h"
#include "stack.h"

#include <stdio.h>

/**
 * @brief Teste l'empilage dans une pile
 *
 * @return le test a réussi
 */
bool testPushStack(void) {
	stack_t stack;
	bool result = true;
	bool aux;
	stackInit(&stack, STACK_MAX_SIZE);

	printf("Test : Empiler dans une pile vide\n");
	stackPush(&stack, 1);
	aux = printResult(stack.top == 0 && stack.data[0] == 1);
	result = result == true ? aux : false;

	printf("Test : Empiler dans une pile non-vide\n");
	stackPush(&stack, 2);
	stackPush(&stack, 3);
	aux = printResult(stack.top == 2 && stack.data[2] == 3);
	result = result == true ? aux : false;

	printf("Test : Empiler dans une pile pleine\n");
	stackPush(&stack, 4);
	stackPush(&stack, 5);
	stackPush(&stack, 999);
	aux = printResult(stack.top == 4 && stack.data[4] == 5);
	result = result == true ? aux : false;

	stackFree(&stack);
	return aux;
}

/**
 * @brief Teste le dépilage d'une pile
 *
 * @return le test a réussi ou pas
 */
bool testPopStack(void) {
	stack_t stack;
	data_stack_t data;
	bool result = true;
	bool aux;
	stackInit(&stack, STACK_MAX_SIZE);

	printf("Test : Dépiler dans une pile vide\n");
	aux = printResult(stackPop(&stack, &data) != OK);
	result = result == true ? aux : false;

	printf("Test : Dépiler dans une pile non-vide\n");
	stackPush(&stack, 1);
	stackPush(&stack, 2);
	stackPop(&stack, &data);
	aux = printResult(data == 2 && stack.top == 0);
	result = result == true ? aux : false;

	printf("Test : Dépiler dans une pile pleine\n");
	stackPush(&stack, 2);
	stackPush(&stack, 3);
	stackPush(&stack, 4);
	stackPush(&stack, 5);
	stackPop(&stack, &data);
	aux = printResult(data == 5 && stack.top == 3);
	result = result == true ? aux : false;

	stackFree(&stack);
	return result;
}

/**
 * @brief Teste la récupération du haut d'une pile
 *
 * @return le test a réussi ou pas
 */
bool testTopStack(void) {
	stack_t stack;
	data_stack_t data;
	bool result = true;
	bool aux;
	stackInit(&stack, STACK_MAX_SIZE);

	printf("Test : Récupérer le haut d'une pile vide\n");
	aux = printResult(stackTop(stack, &data) != OK);
	result = result == true ? aux : false;

	printf("Test : Récupérer le haut d'une pile non-vide\n");
	stackPush(&stack, 1);
	stackPush(&stack, 2);
	stackTop(stack, &data);
	aux = printResult(data == 2 && stack.top == 1);
	result = result == true ? aux : false;

	printf("Test : Récupérer le haut d'une pile pleine\n");
	stackPush(&stack, 3);
	stackPush(&stack, 4);
	stackPush(&stack, 5);
	stackTop(stack, &data);
	aux = printResult(data == 5 && stack.top == 4);
	result = result == true ? aux : false;

	stackFree(&stack);
	return result;
}

/**
 * @brief Teste le redimentionnement de la pile
 *
 * @return le test a réussi ou pas
 */
bool testResizeStack(void) {
	stack_t stack;
	bool result = true;
	stackInit(&stack, STACK_MAX_SIZE);

	printf("Test : Redimensionnement d'une pile\n");
	stackPush(&stack, 1);
	stackPush(&stack, 2);
	stackResize(&stack, 100);
	for (int i = 0; i < 80; i++) {
	   stackPush(&stack, i);
	}
	result = printResult(stack.sizeMax == 100 && stack.data[stack.top] == 79 );
	stackFree(&stack);
	return result;
}

/**
 * @brief Teste l'enfilage dans une file
 *
 * @return le test a réussi ou pas
 */
bool testPushQueue(void) {
	queue_t queue;
	bool result = true;
	bool aux;
	int i = 3;
	queueInit(&queue, QUEUE_MAX_SIZE);

	printf("Test : Enfiler dans une file vide\n");
	queuePush(&queue, 1);
	aux = printResult(queue.deb == 0 && queue.fin == QUEUE_MAX_SIZE && queue.nbElem == 1 && queue.data[0] == 1);
	result = result == true ? aux : false;

	printf("Test : Enfiler dans une file non-vide\n");
	queuePush(&queue, 2);
	aux = printResult(queue.deb == 0 && queue.fin == QUEUE_MAX_SIZE + 1 && queue.nbElem == 2 && queue.data[1] == 2);
	result = result == true ? aux : false;

	printf("Test : Enfiler dans une file pleine\n");
	while (!isQueueFull(queue)) {
		queuePush(&queue, i);
		i++;
	}
	queuePush(&queue, 100);
	aux = printResult(queue.deb == 0 && queue.fin == (2 * QUEUE_MAX_SIZE - 1) && queue.nbElem == QUEUE_MAX_SIZE && isQueueFull(queue));
	result = result == true ? aux : false;

	queueFree(&queue);
	return result;
}

/**
 * @brief Teste le défilage d'une file
 *
 * @return le test a réussi ou pas
 */
bool testPullQueue(void) {
	queue_t queue;
	data_queue_t data;
	bool result = true;
	bool aux;
	int i = 5;
	queueInit(&queue, QUEUE_MAX_SIZE);

	printf("Test : Défiler dans une file vide\n");
	queuePull(&queue, &data);
	aux = printResult(queuePull(&queue, &data) != OK);
	result = result == true ? aux : false;

	printf("Test : Défiler dans une file non-vide\n");
	queuePush(&queue, 1);
	queuePull(&queue, &data);
	aux = printResult(queue.deb == 1 && queue.fin == QUEUE_MAX_SIZE && queue.nbElem == 0);
	result = result == true ? aux : false;

	printf("Test : Défiler dans une file pleine\n");
	while (!isQueueFull(queue)) {
		queuePush(&queue, i);
		i++;
	}
	queuePull(&queue, &data);
	aux = printResult(queue.deb == 2 && queue.fin == 2 * QUEUE_MAX_SIZE && queue.nbElem == QUEUE_MAX_SIZE - 1 && !isQueueFull(queue));
	result = result == true ? aux : false;

	queueFree(&queue);
	return result;
}

/**
 * @brief Teste la récupération de la queue d'une file
 *
 * @return le test a réussi ou pas
 */
bool testEndQueue(void) {
	queue_t queue;
	data_queue_t data;
	bool result = true;
	bool aux;
	int i = 5;
	queueInit(&queue, QUEUE_MAX_SIZE);

	printf("Test : Récupérer la queue d'une file vide\n");
	queueEnd(&queue, &data);
	aux = printResult(queueEnd(&queue, &data) != OK);
	result = result == true ? aux : false;

	printf("Test : Récupérer la queue d'une file non-vide\n");
	queuePush(&queue, 1);
	queuePush(&queue, 2);
	queueEnd(&queue, &data);
	aux = printResult(data == 2 && queue.deb == 0 && queue.nbElem == 2);
	result = result == true ? aux : false;

	printf("Test : Récupérer la queue d'une file pleine\n");
	while (!isQueueFull(queue)) {
		queuePush(&queue, i);
		i++;
	}
	queueEnd(&queue, &data);
	aux = printResult(data == 7 && queue.nbElem == QUEUE_MAX_SIZE);
	result = result == true ? aux : false;

	queueFree(&queue);
	return result;
}

/**
 * @brief Affiche le résultat d'un test "test"
 *
 * @param test Le test
 */
bool printResult(bool test) {
	if (test)
		printf(GRE "\tTest réussi !\n" RESET);
	else
		printf(RED "\tTest non réussi ...\n" RESET);
	printf("=====================================\n");
	return test;
}

/**
 * @brief Lance tous les tests
 *
 */
bool runTests(void) {
	int testSuccesful = 0;
	printf("\n\nLancement des tests :\n");
	testSuccesful += testPushStack() ? 1 : 0;
	testSuccesful += testPopStack() ? 1 : 0;
	testSuccesful += testTopStack() ? 1 : 0;
	testSuccesful += testResizeStack() ? 1 : 0;
	testSuccesful += testPushQueue() ? 1 : 0;
	testSuccesful += testPullQueue() ? 1 : 0;
	testSuccesful += testEndQueue() ? 1 : 0;
	return testSuccesful == 7;
}
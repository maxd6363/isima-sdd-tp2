/**
 * @brief      Définition du type booléen
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#ifndef BOOL_H
#define BOOL_H


/**
 * Déclaration du type booléen
 */
typedef enum {false, true}bool;


#endif
#!/bin/bash


Reset='\033[0m'
Black='\033[0;30m'
Red='\033[0;31m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Blue='\033[0;34m'
Purple='\033[0;35m'
Cyan='\033[0;36m'
White='\033[0;37m'


make clean &>/dev/null
make &>/dev/null
if [[ "$1" == "-v" ]]; then
	valgrind --error-exitcode=1 --leak-check=full ./gestion_file 
else
	valgrind --error-exitcode=1 --leak-check=full ./gestion_file &>/dev/null
fi
result=$?
if [[ result -eq 0 ]]; then
	echo -e $Green"OK !"
else 
	echo -e $Red"There are some leaks or errors !"
fi
var indexSectionsWithContent =
{
  0: "abcdefgilmnopqrstwy",
  1: "qs",
  2: "bcemqst",
  3: "cilmpqrst",
  4: "dfnst",
  5: "d",
  6: "be",
  7: "aefnoqst",
  8: "bcfgmpqrswy"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

